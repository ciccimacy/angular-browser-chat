'use strict';

		angular.module('APP',['firebase']).
		config(function($routeProvider){
			$routeProvider.
			when('/',{templateUrl:'partials/login.html'},{controller:'loginCtrl'}).
			when('/chat/:roomId',{templateUrl:'partials/chat.html'},{controller:'chatCtrl'}).
			otherwise({redirectTo:'/'})
		}).
		controller('loginCtrl',['$scope','enteredName',function($scope,enteredName){
			$scope.room="demo";
			$scope.ok = function(){
					enteredName.setRoom($scope.room);
					enteredName.setUsername($scope.username);
			};
			
		}]).
		controller('chatCtrl', ['$scope','enteredName','$routeParams','$location', function($scope,enteredName,$routeParams,$location){

			$scope.username=enteredName.getUsername();
			$scope.room = $routeParams.roomId;
			$scope.messages = [];
			$scope.myData = new Firebase('https://sizzling-torch-4916.firebaseio.com/Chat/'+$routeParams.roomId);


			$scope.myData.limitToLast(10).on('child_added',function(snapshot){

				if($scope.messages.length < 100){	
				$scope.messages.push(snapshot.val());
			}
			});

			
			$scope.send = function (){
				$scope.myData.push({name:$scope.username,text:$scope.usertext});
				$scope.usertext = "";
			};

			$scope.init = function(){
			if($scope.username == "" || $scope.room == ""){
				$location.path('/');
			}
		};


			
	}]) 
	.service('enteredName', function () {
        var username = "";
        var room = "";
        return {
            getUsername: function () {
                return username;
            },
            setUsername: function(value) {
                username = value;
            },
             getRoom: function () {
                return room;
            },
            setRoom: function(value) {
                room = value;
            }
        };
    });